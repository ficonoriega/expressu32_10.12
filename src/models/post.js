const mongoose = require('mongoose');

let Schema = mongoose.Schema;


const postSchema = Schema({
    title: {type: String, require: true},
    content: {type: String, require: true},
    user: {type: String, require: true},
    created: {type:Date,default:Date.now}
});

//colecciones van en plural
const Post = mongoose.model('posts',postSchema);

module.exports = Post;