//importar el modelo para poder hacer post request


let Post = require('../models/post');
function welcome(req,res){
    res.status(200).send({
            message: "Welcome!"
        });
};

function savePost(req,res){
    let myPost = new Post(req.body); //cree un objeto de tipo post con los objetos de la solicitud
    myPost.save((err,result) => {
        if (err){
            res.status(500).send({message: err});
        } else{
            res.status(200).send({message: result});
        }
    });
};

function listPost(req,res){

    let search = req.params.search;
    //let query; //CONSTRUCCION CONSULTA;
    let queryParam = {};

    if(search){
        //queryParam = { title: search}
        queryParam = {
            $or:[
                {title: {$regex: search, $options: "i"}},
                {content: {$regex: search, $options: "i"}},
                {user: {$regex: search, $options: "i"}}
            ]
        };  
    }; //option is for casesentive
    
    query = Post.find(queryParam).sort('created');
    
    query.exec((err, result) => { //exec genera promesa y se debe colocar un callback

        if (err){
            res.status(500).send({message: err});
        } else{
            res.status(200).send({message: result});
        }

    });
}

function findPost(req,res){
    let id = req.params.id; //busque id en los parámetros de la solicitud
    let query = Post.findById(id);
    query.exec((err,result) => {
        if (err){
            res.status(500).send({message: err});
        } else{
            res.status(200).send({message: result});
        }
    });
};

function deletePost(req,res){
    let id =req.params.id;

    Post.findByIdAndDelete(id, (err, result) => {
        if (err){
            res.status(500).send({message: err});
        } else{
            res.status(200).send({message: result});
        }
    });
};

function updatePost(req,res){

    let id = req.params.id;
    let data = req.body;
    
    Post.findByIdAndUpdate(id, data, {new: true}, (err, result) => { //con new devuelve el valor como quedó
        if (err){
            res.status(500).send({message: err});
        } else{
            res.status(200).send({message: result});
        }
    })

};

module.exports = {welcome, savePost, listPost, findPost, updatePost, deletePost};