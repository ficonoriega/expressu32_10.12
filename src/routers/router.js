//enrutador de la aplicación. Lo que hacía el GETMAPING en java
const {Router} = require('express'); //de express, sólo traer Router
const postController = require('../controllers/postController');

let router = Router();

router.get('/test', postController.welcome); //como en el welcome ya se especica que el método tiene req and res, no se pone acá
router.post('/post/save',postController.savePost); //post antes del save se trata de una publicación, no del método post
router.get('/post/list/:search?',postController.listPost);
router.get('/post/:id',postController.findPost);
router.put('/post/:id',postController.updatePost);
router.delete('/post/:id',postController.deletePost);


module.exports = router;