const express = require("express");
const router = require('./routers/router.js');

let app = express();

//add middleware

app.use(express.json()); //
app.use(express.urlencoded({extended:true}));

app.use((req,res,next) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Headers','GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.header('Allow','GET, POST, PUT, PATCH, DELETE, OPTIONS');
    next();
});

//se coloca esta línea de código después de las cabeceras para que luego
//de asignarlas (cabeceras), enrutar.
//app routers
app.use(router);

module.exports = app;