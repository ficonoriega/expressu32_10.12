//create application

//const express = require('express'); //está agregado a las dependencias
//let app = express();

let app = require('./src/app.js'); //asigno a app la importación de app.js
let connection = require('./src/db/connection')
const PORT = 4000;

app.listen(PORT, () =>{
    console.log("Server is ready in port",PORT);
});